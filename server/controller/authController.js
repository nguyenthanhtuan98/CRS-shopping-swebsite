
 
const Admin = require('.././model/Admin')
const JWT = require('jsonwebtoken');
const createError = require('http-errors');
const { signAccessToken, authuShema } = require('.././validate/auth');
const Products = require("../model/Products");



module.exports.login = (req, res) => {
    authuShema.validateAsync(req.body).then(async(suss) => {
        console.log(suss.email, "sussy")
        const admin = await Admin.findOne({ email: suss.email })
        if (!admin) throw createError(401, 'User not registered');
        const password = await Admin.findOne({ password: suss.password});
        if (!password) throw createError(401, 'Not password');
        const accesToken = await signAccessToken(admin._id)
        res.send({ accesToken: accesToken, admin: admin, isLos:true });
    }).catch((err) => {
        res.send({message: err.message , isLos:false})
    })
}

module.exports.user = async (req, res) => {
    if(req.data == null){
        res.send("null");
    }else{
        const admin = await Admin.findById(req.data.id);
        res.json(admin);
    }
    
}