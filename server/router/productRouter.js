const express = require('express');
const controller = require('../controller/productController');

const router = express.Router();
router.post('/save', controller.save)
router.get("/list", controller.list)
router.get("/delete/:id", controller.delete)
router.get("/views/:id", controller.views)

module.exports = router;