const express = require('express');
const controller = require('../controller/homeController');

const router = express.Router();
router.get('/', controller.index)
router.get('/:id', controller.id)
module.exports = router;