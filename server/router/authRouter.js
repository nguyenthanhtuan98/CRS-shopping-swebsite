const express = require('express');
const controller = require('../controller/authController');
const { verifyAccessToken } = require('../validate/auth');

const router = express.Router();
router.post('/login', controller.login)
router.get('/user', verifyAccessToken, controller.user)
module.exports = router;