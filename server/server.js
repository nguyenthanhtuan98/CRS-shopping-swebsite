const express = require('express');
const app = express();
var port = process.env.PORT || 4000;
const mongoose = require('mongoose');
var createError = require('http-errors');
var cors = require('cors');
const bodyParser = require("body-parser");
require('dotenv').config();
const Products = require('./model/Products')

//
app.use(cors())
    //import router
const homeRouter = require('./router/homeRouter');
const authRouter = require('./router/authRouter');
const productRouter = require('./router/productRouter');
//body-parse
app.use(bodyParser.json({ limit: "10000kb" })); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// CONNECT MONGDB
const url = process.env.MONGO_URL;
const userhost = process.env.DB_USER;
const option = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
}
mongoose.connect(`${userhost}${url}`, option);


app.use('/home', homeRouter);
app.use('/auth', authRouter)
app.use('/products', productRouter);
app.use("/products", productRouter);
app.use("/products", productRouter);



app.post("/products/upload/:id", function(req, res) {
  Products.findById(req.params.id)
    .then(product => {
      product.name = req.body.name;
      product.conten = req.body.conten;

      console.log(product)

      // product
      //   .save()
      //   .then(() => res.json("User upload"))
      //   .catch(err => res.status(400).json("Err: " + err));
    })
    .catch(err => res.status(400).json("Err: " + err));
});

app.listen(port, () => {
    console.log(`Startding... localhost:${port}`)
})