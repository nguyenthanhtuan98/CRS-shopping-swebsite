 
const JWT = require('jsonwebtoken');
const Joi = require('joi');

    const authuShema = Joi.object({
        email: Joi.string()
            .alphanum()
            .min(3)
            .max(30)
            .required(),
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    })



    const signAccessToken = (id) =>{
        return new Promise((resolve, reject) => {
            const options = {
                // expiresIn: '16s',
            }
            JWT.sign({ id }, JSON.stringify(id), options, (err, token) => {
                if (err) console.log("err")
                resolve(token);
            })
        })
    }


    const verifyAccessToken = (req, res, next) => {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) return next(creatError.Unauthorized())
        try {
            const verified = JWT.decode(token)
            req.data = verified;
            next();
        } catch{
            res.status(400).send('Invalid Token');
            req.data = "********";
            next();
        }
    }

module.exports ={
    signAccessToken,
    verifyAccessToken,
    authuShema
}