var mongoose = require('mongoose');

  const personSchema = new mongoose.Schema({
    name: String,
    catory: Array,
    price : Number,
    rate : Number,
    date : String,
    listImage: [ 
    	{
    		url: String,
    		uid: String ,
            name: String ,
            delete_hash: String ,
    	}
    ],
    information: {
      Kho: Number,
      Trademark: String,
      Material: String,
      Origin: String,
      modifier: String,
    },
  });

  // compile our model
  const Products = mongoose.model('Products', personSchema);

module.exports = Products;