const mongoose = require("mongoose");

const personSchema = new mongoose.Schema({
    email: { type: String, required: true },
    password: { type: String, required: true }
});


const  Admin = mongoose.model('Admin', personSchema);

module.exports =  Admin;