import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import 'antd/dist/antd.css';
import './style.css';
import { Table, Switch, Space, Tooltip, Tag, Popconfirm, message } from 'antd';
import {
  DeleteOutlined,
  EditOutlined
} from '@ant-design/icons';

import { useNavigate, Link } from 'react-router-dom';

import * as actionTypes from '../../.././actions/index';
const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
    {
        title: 'Img',
        dataIndex: 'name',
        key: 'name',
    },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    width: '12%',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
    width: '12%',
  },
  {
    title: 'Catory',
    dataIndex: 'catory',
    width: '30%',
    key: 'catory',
  }
];

 


export default function ProductCase() {
  const [checkStrictly, setCheckStrictly] = React.useState(false);
  const [page, setPage] = useState(1);
  const [rowItems, setRowItems] = useState([]);
  const dispatch = useDispatch();
  const getListProducts = (page) => dispatch(actionTypes.GetProductsRequest(page));
  const deleteProductsRequest = (id) => dispatch(actionTypes.DeleteProductsRequest(id));
  const datas = useSelector(state => state.products.data);
  const navigate = useNavigate();

  useEffect(() => {
      getListProducts(page);
  }, [page]);

  const onDelete = (id) =>{
  	console.log("ddekere", id)
  }
  const onEdit = () =>{
    navigate(`/admin/product/case/edit/${rowItems[0]}`, { replace: true });
  }
  const onAdd = () =>{
  	console.log("add")
  }

  //------------


// rowSelection objects indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    const LIST_SELL = []

    for (let i = 0; i < selectedRows.length; i++) {
      LIST_SELL.push(selectedRows[i].id);
    }
    const result = LIST_SELL.filter(sel=> sel);
        setRowItems(result)
  },
  onSelect: (record, selected, selectedRows) => {
    // console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected, selectedRows, changeRows) => {
    // console.log(selected, selectedRows, changeRows);
  },
};

  const deleteAwait = async () =>{
    for (let i = 0; i < rowItems.length; i++) {
      await deleteProductsRequest(rowItems[i])
    }
  }
    
  const confirm = async () => {
    await deleteAwait();
    getListProducts(page);
    
    console.log(page, "page")
    message.success(`Xóa thàng công.... ${rowItems.length} items`);
   
  }

  const cancel = () => {
    message.error('Err Đã Hủy');
  }

var DATA = [];
for (let i = 0; i < datas.length; i++) {
  var LIST_ITEM = {
      key: datas[i]._id, 
      name:datas[i].name, 
      price:`${JSON.stringify(datas[i].price).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}đ `, 
      date:datas[i].date, 
      id:datas[i]._id, 
      rate: datas[i].rate,
      listImage: datas[i].listImage,
      catory:
        <div>
          <Tag color="volcano">{datas[i].catory[0]}</Tag>
          <Tag color="cyan">{datas[i].catory[1]}</Tag>
          <Tag color="geekblue">{datas[i].catory[2]}</Tag>
        </div>,
      children: [
        {
          key: i+1,
          name: `Mô tả: • Synthetic-and-mesh`,
        },
      ]
    }
  DATA
    .push(LIST_ITEM)
}



  return (
    <>
      <Space align="center" style={{ marginBottom: 16 }}>
        Option: {rowItems.length}
      </Space>
      <Space className="icons add">
        <Tooltip title="add" color="green"><Link to="/admin/product/case/add">+</Link></Tooltip>
      </Space>
      <Space className="icons edit" onClick={onEdit}>
        <Tooltip title="edit" color="cyan"><EditOutlined/></Tooltip>
      </Space>
      {rowItems.length === 0 ?
        '':<Popconfirm
            title="Are you sure to delete this task?"
            onConfirm={confirm}
            onCancel={cancel}
            okText="Yes"
            cancelText="No">
            <Space className="icons delete">
              <Tooltip title="delete" color="red"><DeleteOutlined /></Tooltip>
            </Space>
          </Popconfirm>
      }
      
      <Table
        columns={columns}
        rowSelection={{ ...rowSelection, checkStrictly }}
        dataSource={DATA}
      />
    </>
  );
}

