import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import ImgCrop from 'antd-img-crop';
import {
  Form,
  Input,
  Button,
  Cascader,
  DatePicker,
  InputNumber,
  Switch, 
  Upload, 
  Modal, 
  message, 
  Rate, 
  Slider, 
  Radio, 
  Space
} from 'antd';

//test eddit
import {Editor, EditorState} from 'draft-js';
import 'draft-js/dist/Draft.css';


import CollectionCreateForm from './CollectionCreateForm';
import 'antd/dist/antd.css';
import  './style.css';
import * as actionTypes from '../../.././actions/index';

const ReachableContext = React.createContext();
const UnreachableContext = React.createContext();

//test edoter
function MyEditor() {
    const [editorState, setEditorState] = React.useState(
        () => EditorState.createEmpty(),
    );

    return <Editor editorState={editorState} onChange={setEditorState} />;
}

function CaseAdd() {
  const [componentSize, setComponentSize] = useState('default');
  const [visible, setVisible] = useState(false);
  const [modal, contextHolder] = Modal.useModal();
  const dispatch = useDispatch();
  const key = 'updatable';


  //DATA
  const [catory, setCatory] = useState([]);
  const [listImage, SetlistImage] = useState([{
      uid: '-1',
      name: 'image.png',
      delete_hash: 'QkfUTqLN0vNga31',
      url: 'https://i.imgur.com/37ZfMaT.jpg',
  },]);
  const [information, setInformation] = useState([]);

  const handleChangeCatory = (e) =>setCatory(e);
  const onFormLayoutChange = ({ size }) => setComponentSize(size)

  const saveProductRequestAPI = async products => await dispatch(actionTypes.saveProduct(products))

  const config = {
	  title: 'Information!',
	  content: (
	    <div>
	      <ReachableContext.Consumer>{name => `Kho: ${information.Kho}`}</ReachableContext.Consumer>
	      <br />
	      <UnreachableContext.Consumer>{name => `Thương hiệu: ${information.Trademark}`}</UnreachableContext.Consumer>
	      <br />
	      <UnreachableContext.Consumer>{name => `Chất liệu: ${information.Material}`}</UnreachableContext.Consumer>
	      <br />
	      <UnreachableContext.Consumer>{name => `Xuất xứ: ${information.Origin}`}</UnreachableContext.Consumer>
	    </div>
	  ),
	};
	const onCreate = (values) => {
	    setInformation(values);
	    setVisible(false);
	};



  const onFinish = async (values) => {
    const { name, catory, size, price, rate, date } = values;

    const date_format = date._d.toLocaleDateString()
    const newProduct = { name, catory, size, price, rate, date:date_format, listImage, information }
      console.log(listImage)
    if(confirm('The file will be Add. Are you sure?')){ // eslint-disable-line
        await saveProductRequestAPI(newProduct)
    }
    
  };

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };

  //UPLOAD IMG

    const onChange = (params) => {
        console.log(params.file.originFileObj)
            if(params.file.originFileObj == undefined){
                message.loading({ content: 'Đang xóa...', key });
            let config = {
                method: 'delete',
                url: `https://api.imgur.com/3/image/${params.file.delete_hash}`,
                headers: {
                    'Authorization': 'Client-ID acd724cb6563b96',
                }
            }

            axios(config).then((res)=>{
                message.success({ content: 'Xóa thành công!', key, duration: 2 });
                SetlistImage(params.fileList)
            });
        }else{
            message.loading({ content: 'Đang tải ảnh lên...', key });
            const img = params.file.originFileObj;
            let data = new FormData();
            data.append('image', img)

            let config = {
                method: 'post',
                url: 'https://api.imgur.com/3/image',
                headers: {
                    'Authorization': 'Client-ID acd724cb6563b96',
                },
                data:data
            }
            axios(config).then((res)=>{
                console.log(res.data.data,"dsd")
                message.success({ content: 'Tải thành công!', key, duration: 2 });
                SetlistImage([...listImage ,
                    {
                        url : res.data.data.link,
                        name: res.data.data.link,
                        uid: res.data.data.id,
                        delete_hash:res.data.data.deletehash
                    }
                ])
            }).catch((err)=>{
                console.log(err)
            });
        }

    };
    const onPreview = async (file) => {
        let src = file.url;
        if (!src) {
            src = await new Promise((resolve) => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };

  return (
    <>
      <Form {...layout} onFinish={onFinish} validateMessages={validateMessages}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
      >
        <Form.Item label="Form Size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item 
          label="Name" 
          name="name"
          rules={[
            {
              required: true,
            },
          ]}>
          <Input/>
        </Form.Item>
        <Form.Item 
          label="Catory" 
          name="catory"
          rules={[
            {
              required: true,
            },
          ]}>
          <Cascader
            onChange={handleChangeCatory}
            options={[
              {
                value: 'Giày',
                label: 'Giày',
                children: [
                  {
                    value: 'Nam',
                    label: 'Nam',
                    children: [
	                  {
	                    value: 'Gìay adidas',
	                    label: 'Gìay adidas',
	                  },
	                  {
	                    value: 'Gìay sanck',
	                    label: 'Gìay sanck',
	                  },
	                ],
                  },
                  {
                    value: 'Nữ',
                    label: 'Nữ',
                    children: [
	                  {
	                    value: 'Gìay cao gót',
	                    label: 'Gìay cao gót',
	                  },
	                  {
	                    value: 'Gìay sandabl',
	                    label: 'Gìay sandabl',
	                  },
	                ],
                  },

                ],
              },
              {
                value: 'Áo',
                label: 'Áo',
                children: [
                  {
                    value: 'Nam',
                    label: 'Nam',
                    children: [
	                  {
	                    value: 'áo thun',
	                    label: 'áo thun',
	                  },
	                  {
	                    value: 'áo kaki',
	                    label: 'áo kaki',
	                  },
	                ],
                  },
                  {
                    value: 'Nữ',
                    label: 'Nữ',
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        
    	{catory[0] === 'Giày' ?
    		<Form.Item 
          label="Size" 
          name="size"
          rules={[
            {
              required: true,
            },
          ]}>
		        <Slider
		          marks={{
		            20: '20',
		            40: '40',
		            60: '60',
		            80: '80',
		            100: '**',
		          }}
		        />
		    </Form.Item>:''
        }
        {catory[0] === 'Áo' ?
		    <Form.Item
		        name="size"
		        label="Size"
		        rules={[
		          {
		            required: true,
		            message: 'Please pick an item!',
		          },
		        ]}
		      >
		        <Radio.Group>
		          <Radio.Button value="M">M</Radio.Button>
		          <Radio.Button value="L">L</Radio.Button>
		          <Radio.Button value="XL">XL</Radio.Button>
		          <Radio.Button value="XXL">XXL</Radio.Button>
		        </Radio.Group>
		    </Form.Item>:''
	    }
	      
        <Form.Item label="DatePicker" name="date"
          rules={[
            {
              required: true,
            },
          ]}>
          <DatePicker/>
        </Form.Item>
        <Form.Item label="Price" name="price"
          rules={[
            {
              required: true,
            },
          ]}>

          <InputNumber style={{width:'145px'}}
            defaultValue={10000}
            formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
          />
        </Form.Item>
        
        <Form.Item name="rate" label="Rate"
          rules={[
            {
              required: true,
            },
          ]}>
        	<Rate/>
      	</Form.Item>

        <Form.Item label="Image View">
            <ImgCrop rotate>
                <Upload
                    listType="picture-card"
                    fileList={listImage}
                    onChange={onChange}
                    onPreview={onPreview}
                >
                    {listImage.length < 5 && '+ Upload'}
                </Upload>
            </ImgCrop>
        </Form.Item>

        <Form.Item label="Information">
            { information.modifier ? 
            	<ReachableContext.Provider value="Light">
      				<Space>
		            	<Button
				          onClick={() => {
				            modal.error(config);
				          }}
				        >
				          Đã khởi tạo information
				        </Button> 
				    </Space>
				    {contextHolder}
      				<UnreachableContext.Provider value="Bamboo" />
				</ReachableContext.Provider>:
		      <div>
		      	<Button
			        type="primary"
			        onClick={() => {
			          setVisible(true);
			        }}
			      >
			        Information
			      </Button>
			      <CollectionCreateForm
			        visible={visible}
			        onCreate={onCreate}
			        onCancel={() => {
			          setVisible(false);
			        }}
			    />
		      </div>
		 	}
		  </Form.Item>



        <Form.Item label="Sale">
          <Switch />
        </Form.Item>
        

        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
          <Button type="primary" htmlType="submit">Save</Button>
        </Form.Item>
      </Form>
    </>
  ); 
};

export default CaseAdd;
