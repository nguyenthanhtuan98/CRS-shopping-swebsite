import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import {
  Form,
  Input,
  Modal,
  message,
  Radio,
  InputNumber
} from 'antd';

const CollectionCreateForm = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  const key = 'updatable';

  const handleOke = () =>{
  	message.loading({ content: 'Đang khởi tạo...', key });
    form
      .validateFields()
      .then((values) => {
      	message.success({ content: 'Khởi tạo thành công!', key, duration: 2 });
        form.resetFields();
        onCreate(values);
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  }
  return (
    <Modal
      visible={visible}
      title="Information product"
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={handleOke}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="Kho"
          label="Kho"
          type="InputNumber"
          rules={[
            {
              required: true,
              message: 'Please input the title of collection!',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Form.Item name="Trademark" label="Thương hiệu">
          <Input type="textarea" />
        </Form.Item>
        <Form.Item name="Material" label="Chất liệu">
          <Input type="textarea" />
        </Form.Item>
        <Form.Item name="Origin" label="Xuất xứ">
          <Input type="textarea" />
        </Form.Item>
        <Form.Item name="modifier" className="collection-create-form_last-form-item">
          <Radio.Group>
            <Radio disabled={true} defaultValue={true} value={true}>Private</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default CollectionCreateForm;