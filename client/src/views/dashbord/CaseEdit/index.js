import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { UploadOutlined } from '@ant-design/icons';
import { useNavigate, useParams } from 'react-router-dom';
import {
  Form,
  Input,
  Button,
  Cascader,
  DatePicker,
  InputNumber,
  Switch, 
  Upload, 
  Modal, 
  message, 
  Rate, 
  Slider, 
  Radio, 
  Space
} from 'antd'; 

import CollectionCreateForm from './CollectionCreateForm';
import 'antd/dist/antd.css';
import * as actionTypes from '../../.././actions/index';

const ReachableContext = React.createContext();
const UnreachableContext = React.createContext();

const CaseEdit = () => {
  const [componentSize, setComponentSize] = useState('default');
  const [visible, setVisible] = useState(false);
  const [modal, contextHolder] = Modal.useModal();
  const dispatch = useDispatch();
  const key = 'updatable';
  const navigate = useNavigate();
  const params = useParams();


  //DATA
  const [catory, setCatory] = useState([]);
  const [listImage, SetlistImage] = useState([]);
  const [information, setInformation] = useState([]);
  const [data, setData] = useState([{name:'dasd'}]);

  const handleChangeCatory = (e) =>setCatory(e);

  const getViewsProductRequestAPI = async id => await dispatch(actionTypes.GetViewsProductsRequest(id))
  
  const datas = useSelector(state => state.products.data);

  useEffect( async () => {
     await getViewsProductRequestAPI(params)
  }, []);

  const config = {
	  title: 'Information!',
	  content: (
	    <div>
	      <ReachableContext.Consumer>{name => `Kho: ${information.Kho}`}</ReachableContext.Consumer>
	      <br />
	      <UnreachableContext.Consumer>{name => `Thương hiệu: ${information.Trademark}`}</UnreachableContext.Consumer>
	      <br />
	      <UnreachableContext.Consumer>{name => `Chất liệu: ${information.Material}`}</UnreachableContext.Consumer>
	      <br />
	      <UnreachableContext.Consumer>{name => `Xuất xứ: ${information.Origin}`}</UnreachableContext.Consumer>
	    </div>
	  ),
	};
	const onCreate = (values) => {
	    setInformation(values);
	    setVisible(false);
	};

  const handleChange = (params) => {
  	message.loading({ content: 'Đang tải ảnh lên...', key });
  	const img = params.target.files[0];
  	let data = new FormData();
  	data.append('image', img)

  	let config = {
  		method: 'post',
  		url: 'https://api.imgur.com/3/image',
  		headers: {
          'Authorization': 'Client-ID 7800c611f11e598',
        },
        data:data
  	}
  	axios(config).then((res)=>{
  		console.log(res.data.data)
  		message.success({ content: 'Tải thành công!', key, duration: 2 });
		SetlistImage([...listImage , 
			{
				url : res.data.data.link, 
				name: res.data.data.link,
				uid: res.data.data.id,
			}
		])
	});

  };


  const onFinish = async (values) => {
    const { name, catory, size, price, rate, date } = values;

    const date_format = date._d.toLocaleDateString()
    const newProduct = { name, catory, size, price, rate, date:date_format, listImage, information }
    if(confirm('The file will be Add. Are you sure?')){ // eslint-disable-line
        // await saveProductRequestAPI(newProduct)
    }
    
  };

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };

  const showForm = (data) =>{
  }
  
  return (
    <>
      <Form {...layout} onFinish={onFinish} validateMessages={validateMessages}
      >
        <Form.Item 
          label="Name" 
          name="name"
          rules={[
            {
              required: true,
            },
          ]}>
          <Input defaultvalueValue={datas.length == 0 ? 'not' : datas.name} />
        </Form.Item>
        {showForm(data)}
        <Form.Item 
          label="Catory" 
          name="catory"
          rules={[
            {
              required: true,
            },
          ]}>
          <Cascader
            onChange={handleChangeCatory}
            options={[
              {
                value: 'Giày',
                label: 'Giày',
                children: [
                  {
                    value: 'Nam',
                    label: 'Nam',
                    children: [
	                  {
	                    value: 'Gìay adidas',
	                    label: 'Gìay adidas',
	                  },
	                  {
	                    value: 'Gìay sanck',
	                    label: 'Gìay sanck',
	                  },
	                ],
                  },
                  {
                    value: 'Nữ',
                    label: 'Nữ',
                    children: [
	                  {
	                    value: 'Gìay cao gót',
	                    label: 'Gìay cao gót',
	                  },
	                  {
	                    value: 'Gìay sandabl',
	                    label: 'Gìay sandabl',
	                  },
	                ],
                  },

                ],
              },
              {
                value: 'Áo',
                label: 'Áo',
                children: [
                  {
                    value: 'Nam',
                    label: 'Nam',
                    children: [
	                  {
	                    value: 'áo thun',
	                    label: 'áo thun',
	                  },
	                  {
	                    value: 'áo kaki',
	                    label: 'áo kaki',
	                  },
	                ],
                  },
                  {
                    value: 'Nữ',
                    label: 'Nữ',
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        
    	{catory[0] === 'Giày' ?
    		<Form.Item 
          label="Size" 
          name="size"
          rules={[
            {
              required: true,
            },
          ]}>
		        <Slider
		          marks={{
		            20: '20',
		            40: '40',
		            60: '60',
		            80: '80',
		            100: '**',
		          }}
		        />
		    </Form.Item>:''
        }
        {catory[0] === 'Áo' ?
		    <Form.Item
		        name="size"
		        label="Size"
		        rules={[
		          {
		            required: true,
		            message: 'Please pick an item!',
		          },
		        ]}
		      >
		        <Radio.Group>
		          <Radio.Button value="M">M</Radio.Button>
		          <Radio.Button value="L">L</Radio.Button>
		          <Radio.Button value="XL">XL</Radio.Button>
		          <Radio.Button value="XXL">XXL</Radio.Button>
		        </Radio.Group>
		    </Form.Item>:''
	    }
	      
        <Form.Item label="DatePicker" name="date"
          rules={[
            {
              required: true,
            },
          ]}>
          <DatePicker/>
        </Form.Item>
        <Form.Item label="Price" name="price"
          rules={[
            {
              required: true,
            },
          ]}>

          <InputNumber style={{width:'145px'}}
            defaultValue={10000}
            formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
          />
        </Form.Item>
        
        <Form.Item name="rate" label="Rate"
          rules={[
            {
              required: true,
            },
          ]}>
        	<Rate/>
      	</Form.Item>

        <Form.Item label="Image View">
	        <div className="button-wrapper">
			  <span className="label">
			    <UploadOutlined /> &nbsp;Upload File 
			  </span>
			  <input type="file" onChange={handleChange} name="upload" id="upload" className="upload-box" placeholder="Upload File" />
			</div>

	        <Upload fileList={listImage} />
        </Form.Item>

        <Form.Item label="Information">
            { information.modifier ? 
            	<ReachableContext.Provider value="Light">
      				<Space>
		            	<Button
				          onClick={() => {
				            modal.error(config);
				          }}
				        >
				          Đã khởi tạo information
				        </Button> 
				    </Space>
				    {contextHolder}
      				<UnreachableContext.Provider value="Bamboo" />
				</ReachableContext.Provider>:
		      <div>
		      	<Button
			        type="primary"
			        onClick={() => {
			          setVisible(true);
			        }}
			      >
			        Information
			      </Button>
			      <CollectionCreateForm
			        visible={visible}
			        onCreate={onCreate}
			        onCancel={() => {
			          setVisible(false);
			        }}
			    />
		      </div>
		 	}
		  </Form.Item>
        <Form.Item label="Sale">
          <Switch />
        </Form.Item>
        

        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
          <Button type="primary" htmlType="submit">Save</Button>
        </Form.Item>
      </Form>
    </>
  ); 
};

export default CaseEdit;
