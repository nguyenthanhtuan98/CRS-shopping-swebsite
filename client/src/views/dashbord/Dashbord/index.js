import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { useSelector, useDispatch } from 'react-redux';
import { Timeline, Progress } from 'antd';

import * as actionTypes from '../../.././actions/index';

export default function Dashbord() {
	const dispatch = useDispatch();
	const getListProducts = (page) => dispatch(actionTypes.GetProductsRequest(page));
	const datas = useSelector(state => state.products.data);

	useEffect(() => {
      getListProducts(1);
  	}, []);
	return (
	    <div className="row custom_row">
	        <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom_col">
	        	<h3>Operation status</h3>
	        	<br/>
	        	<Timeline>
			    {datas
			    	.slice(0,5)
			    	.map((e)=>
			    	<Timeline.Item color="green">{`Create a ${e.name} ${e.date}`}</Timeline.Item>
			    )}
			    <Timeline.Item color="red">
			      <p>Server err network 1</p>
			    </Timeline.Item>
			    <Timeline.Item>
			      <p>Tuan just ordered 2p</p>
			      <p>Vinh just ordered 10p</p>
			      <p>Nam just ordered 35p</p>
			    </Timeline.Item>
			  </Timeline>
	    	</div>
	    	<div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	    		<h3>Total Products</h3>
	    		<Progress type="circle" percent={datas.length} format={percent => `${percent} Products`} />
	    		<br/>
	    		<br/>
	    		<h3>Percentage index</h3>
	    		<br/>
	    		<Progress percent={30} size="small" />
			    <Progress percent={50} size="small" status="active" />
			    <Progress percent={10} size="small" />
			    <Progress percent={10} size="small" />
	    	</div>
	    </div>
			
	)
}

