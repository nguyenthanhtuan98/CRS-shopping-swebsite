import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './style.css';
import { Menu } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Dropdown, Button, Space } from 'antd';


const { SubMenu } = Menu;


export default function Navbar({user}) {
  const [current, setCurrent] = useState('mail');

  const handleClick = e => {
    setCurrent(e.key)
  };
  const logout = () =>{
    localStorage.removeItem('token');
    localStorage.removeItem('email');
    window.location = "/auth/login"
  }
  const menu = (
    <Menu>
      <Menu.Item onClick={logout}>
        Logout
      </Menu.Item>
    </Menu>
  );

    return (
      <div className="root_navbar">
        <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
          <Menu.Item key="mail" icon={<MailOutlined />}>
            Navigation One
          </Menu.Item>
          <Menu.Item key="app" disabled icon={<AppstoreOutlined />}>
            Navigation Two
          </Menu.Item>
          <SubMenu key="SubMenu" icon={<SettingOutlined />} title="Navigation Three - Submenu">
            <Menu.ItemGroup title="Item 1">
              <Menu.Item key="setting:1">Option 1</Menu.Item>
              <Menu.Item key="setting:2">Option 2</Menu.Item>
            </Menu.ItemGroup>
            <Menu.ItemGroup title="Item 2">
              <Menu.Item key="setting:3">Option 3</Menu.Item>
              <Menu.Item key="setting:4">Option 4</Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
          
          <Menu.Item key="alipay" style={{float: 'right'}}>
          <Space wrap>
            <Dropdown overlay={menu} placement="topRight">
              <div>
                <span>Chào {user}</span> &nbsp;
                <Avatar
                  style={{
                    backgroundColor: '#87d068',
                  }}
                  icon={<UserOutlined />}
                />
                </div>
            </Dropdown>
          </Space>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
