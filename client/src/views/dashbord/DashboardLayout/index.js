import React, { useState, useEffect  } from 'react';
import { Outlet } from 'react-router-dom';
import Topbar from './Topbar';
import Navbar from './Navbar';
import Permission from '../../.././component/Permission';
import './style.css';

function DashboardLayout() {
	const [user, setUser] = useState('');

	useEffect(() => {
		let user = localStorage.getItem("email");
    	setUser(user)
  	}, []);
    return (
    	<div className="root_layout">
	      	<Navbar user={user}/>

	    	<div className='root'>
		      <Topbar />
		      <div className='wrapper'>
		        <div className='contentContainer'>
		          <div className='content'>
		            <Outlet />
		          </div>
		        </div>
		      </div>
	      </div>
      </div>
    );
}

export default Permission(DashboardLayout, localStorage.getItem("token"));


