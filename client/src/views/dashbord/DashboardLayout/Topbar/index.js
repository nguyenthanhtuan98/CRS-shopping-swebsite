import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './style.css';
import { Menu, Button } from 'antd';
import {
  AppstoreOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PieChartOutlined,
  DesktopOutlined,
  ContainerOutlined,
  MailOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';

const { SubMenu } = Menu;

export default function Topbar() {
	const [collapsed, setCollapsed] = useState(false);

	const toggleCollapsed = () => {
	    setCollapsed(!collapsed)
	};

    return (
      <div style={{ width: 256}}>
        <Menu
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="inline"
          inlineCollapsed={collapsed}
        >
          <Menu.Item key="1" icon={<PieChartOutlined />}>
            <Link to="/admin/dashboard">Dashbord</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<DesktopOutlined />}>
            Option 2
          </Menu.Item>
          <Menu.Item key="3" icon={<ContainerOutlined />}>
            Option 3
          </Menu.Item>
          <SubMenu key="sub1" icon={<MailOutlined />} title="Products">
            <Menu.Item key="5"><Link to="/admin/product/case">Case</Link></Menu.Item>
            <Menu.Item key="6"><Link to="/admin/product/order">Order</Link></Menu.Item>
            <Menu.Item key="7">Option 7</Menu.Item>
            <Menu.Item key="8">Option 8</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<AppstoreOutlined />} title="User">
            <Menu.Item key="9">Admin</Menu.Item>
            <Menu.Item key="10">User develop</Menu.Item>
            <Menu.Item key="11">User customer</Menu.Item>
          </SubMenu>
        </Menu>
        <Button type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16 }}>
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
        </Button>
      </div>

    );
}