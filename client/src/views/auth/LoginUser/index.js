import React, { useState, useEffect  } from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'antd';
import { PoweroffOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';

import './style.css';
import * as actionTypes from '../../.././actions/index';


export default function LoginUser() {
 const navigate = useNavigate();

  const dispatch = useDispatch();
  const [state, setState] = React.useState({
    email: '',
    password: '',
  });
  const [loadings, setLoadings] = useState([]);
  const loginUserRequestAPI = async user => await dispatch(actionTypes.loginUserRequestAPI(user)) 
  const informationUserRequestAPI = async token => await dispatch(actionTypes.informationUserRequestAPI(token)) 

  useEffect(() => {
    var input = document.querySelector('.pswrd');
    var show = document.querySelector('.show');
    show.addEventListener('click', active);
    function active(){
      if(input.type === "password"){
        input.type = "text";
        show.style.color = "#1DA1F2";
        show.textContent = "HIDE";
      }else{
        input.type = "password";
        show.textContent = "SHOW";
        show.style.color = "#111";
      }
    }
    let token = localStorage.getItem("token");
    informationUserRequestAPI(token)
  }, []);




  // const handleSubmit = async (e) =>{
  // 	e.preventDefault();

  //   loginUserRequestAPI(state)
  // }

  const handleChange = (e) =>{
    setState({ ...state, [e.target.name]: e.target.value });
  }
  
  const enterLoading = index => {
    // navigate('/inspection/auth/login', { replace: true });

    loginUserRequestAPI(state)
    setLoadings(( loadings ) => {
      const newLoadings = [...loadings];
      newLoadings[index] = true;

      return newLoadings

    });
    setTimeout(() => {
      setLoadings(( loadings ) => {
        const newLoadings = [...loadings];
        newLoadings[index] = false;

        return newLoadings
      });
    }, 1000);
  };


  

  return (
    <div className="container-fluid custom_col box">
      <div className="row custom_row cart">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 custom_col">
          <div className="container">
            <header>Login Form</header>
            <form>
              <div className="input-field">
                <input name="email" type="text" onChange={handleChange} value={state.email} required />
                <label>Email or Username</label>
              </div>
              <div className="input-field">
                <input className="pswrd" onChange={handleChange} type="password" value={state.password} name="password" required />
                <span className="show">SHOW</span>
                <label>Password</label>
              </div>
              <div className="button">
                <div className="inner">
                </div>
                <Button type="primary" loading={loadings[0]} onClick={() => enterLoading(0)}>
                  Click me!
                </Button>
              </div>
            </form>
            <div className="auth">
            Or login with</div>
            <div className="links">
              <div className="facebook">
                <i className="fab fa-facebook-square"><span>Facebook</span></i>
              </div>
              <div className="google">
                <i className="fab fa-google-plus-square"><span>Google</span></i>
              </div>
            </div>
            <div className="signup">
              Not a member? <a href="#">Signup now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}