import { Navigate } from 'react-router-dom';
import JWT from 'jsonwebtoken';

var REACT_APP_ADMIN_TOKEN = '6371a2b568ccfcace05c690f'

const Permission = (Component, token) => (props) => {
	if(token == null) return ( <Navigate to="/auth/404" /> );
  	return REACT_APP_ADMIN_TOKEN === JWT.decode(token).id ? ( <Component /> ) : ( <Navigate to="/auth/404" replace /> )
};

export default Permission;