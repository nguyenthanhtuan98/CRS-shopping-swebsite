import { combineReducers } from 'redux';

import user from '.././reducers/User';
import Product from '.././reducers/Product';
const myReducers = combineReducers({
	products: Product,
    User: user,
});
export default myReducers;
