import { message } from 'antd';
import 'antd/dist/antd.css';
import * as actionTypes from '.././constants/actionType';
const initialState = {
    token: null,
    user: []
};
const key = 'updatable';

const user = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.LOGIN_USER: {
            const { userLogin } = action;
            const token = userLogin.accesToken;
            const isLos = userLogin.isLos;
            const user = userLogin.admin;
            if(!isLos){
                message.loading({ content: 'Loading...', key });
                setTimeout(() => {
                    message.error({ content: userLogin.message, key, duration: 2 });
                }, 1000);
                return {
                    ...state,
                    token: userLogin.message,
                    isLos: isLos
                }
            }else{
                localStorage.setItem("token", token);
                localStorage.setItem("email", user.email);
                message.loading({ content: 'Loading...', key });
                setTimeout(() => {
                    message.success({ content: "Login thang cong...", key, duration: 2 });
                    window.location = "/admin/dashboard"
                }, 1000);
                return {
                    ...state,
                    token: token,
                    isLos: isLos
                }
            }
        }
        case actionTypes.INFORMATION_USER: {
            if(action.data == null){
                console.log("null")
            }else{
                window.location = "/admin/dashboard"
                localStorage.setItem("email", action.data.email);
                return {
                    ...state,
                    user: action.data
                };
            }

            
        }
        default: return { ...state };
    }
};

export default user;
