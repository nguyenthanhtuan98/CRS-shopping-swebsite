import { message } from 'antd';
import 'antd/dist/antd.css';
import * as actionTypes from '.././constants/actionType';
const key = 'updatable';

const initialState = {
    data: [],
    status:null,
    lengthData: null
};

const Product = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_LIST_PRODUCTS: {
            return {
                ...state,
                data: action.product
            }
        }
        case actionTypes.SAVE_PRODUCT: {
            console.log(action.status)
            if(action.status === 200){
                message.loading({ content: 'Đang lưu dữ liệu...', key });
                setTimeout(() => {
                    message.success({ content: 'Lưu thành công...', key, duration: 2 });
                    window.location = "/admin/product/case"
                    return {
                        ...state,
                        data: action.status
                    };

                }, 1000);
            }
        }
        case actionTypes.DELETE_LIST_PRODUCTS: {
                return {
                    ...state,
                    status: action.product
                };
        }
        case actionTypes.GET_VIEWS_PRODUCTS: {
                return {
                    ...state,
                    data: action.product
                };
        }
        default: return state;
    }
};

export default Product;
