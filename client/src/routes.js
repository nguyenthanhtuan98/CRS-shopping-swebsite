import React from 'react';
import LoginUser from './views/auth/LoginUser';
import NotFaul from './views/auth/NotFaul';
import CaseAdd from './views/dashbord/CaseAdd';
import CaseEdit from './views/dashbord/CaseEdit';
import Dashbord from './views/dashbord/Dashbord';
import DashboardLayout from './views/dashbord/DashboardLayout';
import ProductCase from './views/dashbord/ProductCase';
const routes = [
  {
    path: 'auth',
    children: [
      {
        path: 'login',
        children: [
          { path: '/', element: <LoginUser /> }
        ]
      },
      {
        path: '404',
        children: [
          { path: '/', element: <NotFaul /> }
        ]
      },
    ]
  },
  {
    path: 'admin',
    element: <DashboardLayout />,
    children: [

      {
        path: 'product',
        children: [
          { path: '/case', 
            element: <ProductCase />, 
          },
        ]
      },
      {
        path: '/product/case/add',
        children: [
          { path: '/', element: <CaseAdd /> },
        ]
      },
      {
        path: '/product/case/edit',
        children: [
          { path: '/:productId', element: <CaseEdit /> },
        ]
      },
      {
        path: 'dashboard',
        element: <Dashbord />
      },
    ]
  }
]


export default routes;
