  
import axios from 'axios';
import * as URL from './contans';

export default function API(endpoint, method, body, token) {
    return axios({
        method: method,
        url: `${URL.API_LOCALHOST}/${endpoint}`,
        data: body,
        headers: { "Authorization": `Bearer ${token}` }
    })
}