
import { message } from 'antd';
import 'antd/dist/antd.css';
import * as actionTypes from '.././constants/actionType';
import API from '.././api/api';
const key = 'updatable';

export const loginUserRequest = userLogin => {
    return {
        type: actionTypes.LOGIN_USER,
        userLogin
    }
};

export const loginUserRequestAPI = user => {
    return dispatch => {
        return API('auth/login', 'POST', user).then(res => {
            dispatch(loginUserRequest(res.data));
        }).catch(err => {
            //err
        })
    }
};


export const informationUserRequest = data => {
    return {
        type: actionTypes.INFORMATION_USER,
        data,
    }
}

export const informationUserRequestAPI = token => {
    return dispatch => {
        return API('auth/user', 'GET', null, token).then(res => {
            dispatch(informationUserRequest(res.data));
        }).catch(err => {
            console.log('err', err);
            localStorage.removeItem('token');
    
        })
    }
};

//-----------------//---------------//


export const seveProductRequest = status => {
    return {
        type: actionTypes.SAVE_PRODUCT,
        status
    }
};


export const saveProduct = products => {
    return dispatch => {
        return API('products/save', 'POST', products).then(res => {
            dispatch(seveProductRequest(res.data));
        }).catch(err => {
            //err
            message.error({ content: 'Request failed with status code 404', key, duration: 2 });
        })
    }
};


export const GetProducts = product => {
    return {
        type: actionTypes.GET_LIST_PRODUCTS,
        product
    }
};
 
export const GetProductsRequest = (page) => {
    return dispatch => {
        return API(`products/list?page=${page}`, 'GET', null).then(res => {
            dispatch(GetProducts(res.data))
        })
    }
};

export const DeleteProducts = product => {
    return {
        type: actionTypes.DELETE_LIST_PRODUCTS,
        product
    }
};

export const DeleteProductsRequest = (id) => {
    return dispatch => {
        return API(`products/delete/${id}`, 'GET', null).then(res => {
            console.log(res, "data delete")
            dispatch(DeleteProducts(res.status))
        })
    }
};


export const GetViewsProducts = product => {
    return {
        type: actionTypes.GET_VIEWS_PRODUCTS,
        product
    }
};
 

export const GetViewsProductsRequest = (id) => {
    console.log(id)
    return dispatch => {
        return API(`products/views/${id.productId}`, 'GET', null).then(res => {
            dispatch(GetViewsProducts(res.data))
        })

    }
};